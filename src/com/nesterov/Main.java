package com.nesterov;

import java.util.Scanner;
import java.util.Queue;    // Импорт интерфейса Queue.
import java.util.LinkedList;  // Импорт класса LinkedList, который реализует интерфейс Queue.

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Создание объекта Scanner для считывания ввода с терминала.
        Queue<Integer> line = new LinkedList<Integer>();  // Создание очереди line для хранения городов маршрута.
        System.out.println("Введите цифры по очереди, тем самым составляя маршрут вашего путешествия (для завершения введите 0)" +
                "\n1. London" +
                "\n2. Singapore" +
                "\n3. NY" +
                "\n4. Sydney" +
                "\n5. Helsinki" +
                "\n6. Mexico City");

        while (true) {  // Бесконечный цикл.
            int input = scanner.nextInt();  // Считывание целого числа из ввода пользователя.
            if (input == 0) {  // Если введен 0, выход из цикла.
                break;
            } else if (input > 6 || input < 0) {  // Если введенное число не в допустимом диапазоне.
                System.out.println("Города с такой цифрой нет. Городов всего 6");  // Вывод сообщения об ошибке.
            } else {
                line.offer(input);  // Добавление введенного числа в очередь.
            }
        }

        System.out.println("Вы поедете:");  // Вывод сообщения.

        while (!line.isEmpty()) {  // Цикл вывода городов из очереди.
            int i = line.poll();  // Извлечение элемента из очереди.
            System.out.print("город " + i + " ---> ");  // Вывод города.
        }
        System.out.println("Возвращаемся домой!");  // Вывод сообщения.
    }
}
